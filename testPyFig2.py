import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import seaborn as sns
import colorsys

# data loading
df = pd.read_csv("file.csv", index_col=0) 

colors = colorsys.hsv_to_rgb(0,0,0.5)
cmap = sns.light_palette(colors, input='rgb', reverse=False, as_cmap=True)

#print(df > 5)
print(df.mask(((df == df) | df.isnull()) & (df.columns != "att1")))
# plotting
with sns.axes_style('white'):
	fig,ax = plt.subplots()
	cmap, norm = mcolors.from_levels_and_colors([0,5, 100], ['white', 'grey'])
	ax.matshow(df.mask(((df > 5) | df.isnull()) & (df.columns != "att1")), 
			   cmap=cmap) # You can change the colormap here

	cmap, norm = mcolors.from_levels_and_colors([0,0.8, 100], ['white', 'grey'])
	ax.matshow(df.mask(((df > 0.8) | df.isnull()) & (df.columns != "att2")), 
			   cmap=cmap)

	cmap, norm = mcolors.from_levels_and_colors([0,0.1, 100], ['white', 'grey'])
	ax.matshow(df.mask(((df > 0.1) | df.isnull()) & (df.columns != "att3")), 
			   cmap=cmap)
plt.xticks(range(3), df.columns)
plt.yticks(range(4), df.index)
plt.show()
