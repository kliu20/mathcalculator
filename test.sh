#!/bin/bash

in_dir=data/sunflow/fig/

eps=$(ls ${in_dir} | grep "\.eps")

for file in ${eps}; do
	echo ${file%*.eps}
done
