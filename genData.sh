#!/bin/bash

DEBUG=false
ENERGY=0;
POWER=1; 
COUNTER_RATE_BY_TIME=2;
TIME=3;
FREQUENCY=5;
COUNTER1=4;
COUNTER2=5;
COUNTER_RATE=6;
SINGLE_ATTR_STR='single'
ALL_ATTR_STR='all'
path='data/sunflow';
#CLASSPATH=".:lib/commons-math3-3.6.1.jar:bin"
CLASSPATH="target/mathCalc-0.0.1-SNAPSHOT.jar"
inputFiles=('branch-misses.csv' 'cache-misses.csv' 'cpu-clocks.csv' 'cpu-cycles.csv' 'page-faults.csv')
singleCounterFile=('cpu-clocks.csv' 'cpu-cycles.csv' 'page-faults.csv')
doubleCounterFile=('branch-misses.csv' 'cache-misses.csv')
outputFiles=('branch-misses' 'cache-misses' 'cpu-clocks' 'cpu-cycles' 'page-faults')
preDefCorCounters=($ENERGY $POWER $TIME $FREQUENCY)
params=('energy' 'power' 'time')
showAttrsDouble=('counter1' 'counter2' 'counterRate')
showAttrsSingle=('counter1' 'counterRate')

stat_csv_generator() {
	i=0
	for file in "${inputFiles[@]}"
	do
		java -cp ${CLASSPATH} kenan.Harness $1 -i ${path}/rawData/${file} > ${path}/$1/${outputFiles[$i]}\_$1.csv
		((i++))
	done	
}

data_mining_csv_generator() {
	java -Dtest=false -Dfigure=false -cp $CLASSPATH  kenan.Harness $1 -i $path/rawData/$5 -p $2 -s $3 $4 > ${path}/$1/$2\_$3\_$7\_$6.csv
}

data_mining_one_attr() {
	for param in "${params[@]}"
	do
		i=0
		for input in "${doubleCounterFile[@]}"
		do
			for attr in "${showAttrsDouble[@]}"
			do
				if [ ${attr} == 'counter1' ]; then
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}
				elif [ ${attr} == 'counter2' ]; then
					IFS='-' read -ra NAMES <<< "${outputFiles[$i]}"
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${NAMES[0]}-references
				elif [ ${attr} == 'counterRate' ]; then
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}Rate
				else
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${attr}
				fi
			done
			((i++))
		done

		for input in "${singleCounterFile[@]}"
		do
			for attr in "${showAttrsSingle[@]}"
			do
				if [ ${attr} == 'counter1' ]; then
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}
				elif [ ${attr} == 'counter2' ]; then
					IFS='-' read -ra NAMES <<< "${outputFiles[$i]}"
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${NAMES[0]}-references
				elif [ ${attr} == 'counterRate' ]; then
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}Rate
				else
					data_mining_csv_generator ${1} ${param} ${param} ${attr} ${input} ${outputFiles[$i]} ${attr}
				fi
			done
			((i++))
		done
	done
}

data_mining_all_attr() {
	for param in "${params[@]}"
	do
		i=0
		for input in "${doubleCounterFile[@]}"
		do
			for attr in "${showAttrsDouble[@]}"
			do
				if [ ${attr} == 'counter1' ]; then
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}
				elif [ ${attr} == 'counter2' ]; then
					IFS='-' read -ra NAMES <<< "${outputFiles[$i]}"
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${NAMES[0]}-references
				elif [ ${attr} == 'counterRate' ]; then
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}Rate
				else
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${attr}
				fi

			done
			((i++))
		done

		for input in "${singleCounterFile[@]}"
		do
			for attr in "${showAttrsSingle[@]}"
			do
				if [ ${attr} == 'counter1' ]; then
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}
				elif [ ${attr} == 'counter2' ]; then
					IFS='-' read -ra NAMES <<< "${outputFiles[$i]}"
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${NAMES[0]}-references
				elif [ ${attr} == 'counterRate' ]; then
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${outputFiles[$i]}Rate
				else
					data_mining_csv_generator ${1} ${ALL_ATTR_STR} ${param} ${attr} ${input} ${outputFiles[$i]} ${attr}
				fi
			done
			((i++))
		done
	done
}

#----------------------Build mathCalculator-----------------------------------
#ant build
mvn clean package 
#data_mining_csv_generator centroid energy energy counter1 branch-misses.csv branch-misses

#----------------------Calculate mean value-----------------------------------
 stat_csv_generator "mean"
# 
##----------------------Calculate standard deviation---------------------------
 stat_csv_generator "sd"

#----------------------Calculate correlation coefficient----------------------
#Using one dimension of X-means (energy, power, time)
data_mining_one_attr "correlation"
#Using all dimensions of X-means (energy, power, time, counter1, frequency, rate and (or counter2))
data_mining_all_attr "correlation"

#----------------------Calculate centroids by k means-------------------------
#Using one dimension of K-means (energy, power, time)
data_mining_one_attr "centroid"
##Using all dimensions of K-means (energy, power, time, counter1, frequency, rate and (or counter2))
data_mining_all_attr "centroid"

