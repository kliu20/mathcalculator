#!/bin/bash

DEBUG=false
ENERGY=0;
POWER=1; 
COUNTER_RATE_BY_TIME=2;
TIME=3;
FREQUENCY=5;
COUNTER1=4;
COUNTER2=5;
COUNTER_RATE=6;
SINGLE_ATTR_STR='single'
ALL_ATTR_STR='all'
path='data/sunflow';
#CLASSPATH=".:lib/commons-math3-3.6.1.jar:lib/smile-core-1.3.0.jar:bin"
CLASSPATH="target/mathCalc-0.0.1-SNAPSHOT.jar"
inputFiles=('branch-misses.csv' 'cache-misses.csv' 'cpu-clocks.csv' 'cpu-cycles.csv' 'page-faults.csv')
singleCounterFile=('cpu-clocks.csv' 'cpu-cycles.csv' 'page-faults.csv')
doubleCounterFile=('branch-misses.csv' 'cache-misses.csv')
outputFiles=('branch-misses' 'cache-misses' 'cpu-clocks' 'cpu-cycles' 'page-faults')
preDefCorCounters=($ENERGY $POWER $TIME $FREQUENCY)
params=('energy' 'power' 'time')
showAttrsDouble=('counter1' 'counter2' 'counterRate')
showAttrsSingle=('counter1' 'counterRate')

mvn clean package 
#ant build

java -Dtest=false -Dfigure=true -cp $CLASSPATH  kenan.Harness centroid -i $path/rawData/branch-misses.csv -p energy -s energy counter1 -f energy counter1 time
#java -Dtest=false -Dfigure=true -cp $CLASSPATH  kenan.Harness centroid -i $path/rawData/cpu-clocks.csv -p energy -s energy counter1 -f energy counter1 time
#java -Dtest=false -Dfigure=true -cp $CLASSPATH  kenan.Harness centroid -i $path/rawData/cache-misses.csv -p energy -s energy counter1 -f energy counter1 time
