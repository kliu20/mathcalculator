package kenan.math.mean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import kenan.math.RawDataOffset;
import kenan.math.StatisticsCSV;

public class MeanCSV extends StatisticsCSV {
	
	double counter1 = 0.0d;
	double counter2 = 0.0d;
	double energy = 0.0d;
	double power = 0.0d;
	double time = 0.0d;
	double counterRate = 0.0d;
	double counterRateByTime = 0.d;
	
	/**
	 * Print mean value for each frequency
	 */
	public void printMeanForFreq(String methodName, int freq) {
		System.out.print(methodName + "," + freq + "," + energy/colCounter + "," + power/colCounter + "," + counter1/colCounter 
				+ "," + counter2/colCounter + "," + counterRate/colCounter + "," + counterRateByTime/colCounter
				+ "," + time/colCounter);
		System.out.println();
	}
	
	/**
	 * Calculate mean and print out
	 * @param list lists of csv data
	 */
	public void getMean(List<String> list) {
		String data;
		String[] columns;

		data = list.get(1);
		columns = data.split(",");
		int prevFreq = Integer.parseInt(columns[0]);
		
		getMethodMap(list);
		
		Iterator it = mthInfoMap.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, List<String>> pair = (Map.Entry<String, List<String>>)it.next();
			List<String> singleMthList = pair.getValue();
			//Reset variables
			data = singleMthList.get(0);
			columns = data.split(",");
			prevFreq = Integer.parseInt(columns[0]);
			resetVar();
		
			for(int i = 0; i < singleMthList.size(); i++) {
				data = singleMthList.get(i);
				columns = data.split(",");
				if(prevFreq == Integer.parseInt(columns[0])) {
					//TODO
					colCounter++;
					prevFreq = Integer.parseInt(columns[0]);
					energy += Double.parseDouble(columns[columns.length - PKG_ENER_NEG]);
					power += Double.parseDouble(columns[columns.length - PKG_POWER_NEG]);
					counter1 += Double.parseDouble(columns[COUNTER1_POS]);
					counterRateByTime += Double.parseDouble(columns[columns.length - COUNTER_RATE_TIME_NEG]);
					time += Double.parseDouble(columns[columns.length - TIME_NEG]);
					if(columns.length == HAS_RATE_NUM) {
						counter2 += Double.parseDouble(columns[COUNTER2_POS]);
						counterRate += Double.parseDouble(columns[columns.length - COUNTER_RATE_NEG]);
					}
					if(i == singleMthList.size() - 1) {
						printMeanForFreq(pair.getKey(), prevFreq);
					}
				} else {
	
					//If one frequency only has one column, discard it.
					if(colCounter == 1) {
						prevFreq = Integer.parseInt(columns[0]);
						continue;
					}
					//Print mean value
					printMeanForFreq(pair.getKey(), prevFreq);
					//Move to the next frequency
					colCounter = 1;
	
					prevFreq = Integer.parseInt(columns[0]);
					//Reset for the new frequency
					energy = Double.parseDouble(columns[columns.length - PKG_ENER_NEG]);
					power = Double.parseDouble(columns[columns.length - PKG_POWER_NEG]);
					counter1 = Double.parseDouble(columns[COUNTER1_POS]);
					counterRateByTime = Double.parseDouble(columns[columns.length - COUNTER_RATE_TIME_NEG]);
					time = Double.parseDouble(columns[columns.length - TIME_NEG]);
					if(columns.length == HAS_RATE_NUM) {
						counter2 = Double.parseDouble(columns[COUNTER2_POS]);
						counterRate = Double.parseDouble(columns[columns.length - COUNTER_RATE_NEG]);
					}
				}
			}
		}
	}
	
	/**
	 * Reset variables
	 */
	public void resetVar() {
		counter1 = 0.0d;
		counter2 = 0.0d;
		energy = 0.0d;
		power = 0.0d;
		time = 0.0d;
		counterRate = 0.0d;
		counterRateByTime = 0.d;
	}
}
