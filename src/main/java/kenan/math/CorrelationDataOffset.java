package kenan.math;

public interface CorrelationDataOffset {
	public static final int ENERGY					= 0;
	public static final int POWER 					= 1; 
	public static final int COUNTER_RATE_BY_TIME	= 2;
	public static final int TIME					= 3;
	public static final int COUNTER1				= 4;
	public static final int FREQUENCY				= 5;
	public static final int COUNTER2				= 6;
	public static final int COUNTER_RATE			= 7;
	public static final int NUM_WITH_RATE			= 8;
	public static final int NUM_WITHOUT_RATE		= 6;
}
