package kenan.math;

public interface RawDataOffset {
	public static final int FREQUENCY_POS				= 0;
	public static final int METHODNAME_POS				= 4;
	public static final int COUNTER1_POS				= 6;
	public static final int COUNTER2_POS				= 7;
	public static final int COUNTER_RATE_NEG			= 3;
	public static final int COUNTER_RATE_TIME_NEG		= 2;
	public static final int PKG_POWER_NEG				= 4;
	public static final int PKG_ENER_NEG				= 5;
	public static final int TIME_NEG					= 1;
	public static final int HAS_RATE_NUM				= 15;
	public static final int NO_RATE_NUM					= 14;
	
}
