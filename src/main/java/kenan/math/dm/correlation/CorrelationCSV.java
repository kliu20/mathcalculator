package kenan.math.dm.correlation;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

//import smile.plot.Palette;
import kenan.math.CorrelationDataOffset;
import kenan.math.RawDataOffset;
import kenan.math.dm.cluster.ClusterCalc;



public class CorrelationCSV extends ClusterCalc implements RawDataOffset, CorrelationDataOffset {
	

//	private HashMap<Double, Boolean> isCalculated = new HashMap<Double, Boolean>();
	
	/**
	 * Calculate correlation coefficient and print out
	 * @param list list that contains contents of csv file
	 * @param parameter parameters that need to be calculated in K-means
	 * @param showAttr parameters that need to be showed in csv file 
	 * @param figures parameters that need to be showed in 3D figures
	 */
	public void getCorrelation(List<String> list, List<String> showAttr, List<String> parameter, List<String> figures) {
		int mthMapIndex = 0;
		
		int[] attrsIndex = new int[parameter.size()];
		int[] showAttrIndex = new int[showAttr.size()];
		
		//Get the index for the counters that needs to be calculated
		initArgsIndexMap(attrsIndex, showAttrIndex, parameter, showAttr, hasRateCol);
		
		getMethodMap(list);
		Iterator it = mthInfoMap.entrySet().iterator();
		printCCTitle();
		while(it.hasNext()) {
			Map.Entry<String, List<String>> pair = (Map.Entry<String, List<String>>)it.next();
			
			//If the count of rows of the method is too small, skip it
			if(!rowCountCheck(mthMapIndex)) continue;
			mthMapIndex++;
			
			preProcessCentroids(pair, showAttr, parameter, figures, attrsIndex);
			
			double[][] CC = calcCor(showAttrIndex);
			printCCForFreq(pair.getKey(), CC);
			
			//Clear variables for each method
			clearVars();
		}
	}
	
	/**
	 * Print title for Correlation Coefficient
	 */
	public void printCCTitle() {
		System.out.println("methodName,centroids_ID,PearsonsCorrelation,SpearmansCorrelation");
	}
	
	/**
	 * Get correlation coefficient result for different frequency with varied centroids
	 * @param showAttrIndex index of attributes to be printed out
	 * @return results of Correlation Coefficient with result[centroidID][algorithmID].
	 * centroidID is the ID of centroid
	 * algorithmID is the ID of CC algorithm:
	 * 		0 is PearsonCorrelation
	 * 		1 is SpearmansCorrelation
	 */
	public double[][] calcCor(int[] showAttrIndex) {
		
		double[] xArray = new double[freqCounter];
		double[] yArray = new double[freqCounter];
		double[][] results;
		
		int varNumCents = 0;
		int maxClusterNum = freqCentroidsMap[0].length;
		int corClusterNum;
		
		//Get max number of centroids
		for(int i = 0; i < freqCounter; i++) {
			if(maxClusterNum < freqCentroidsMap[i].length) {
				maxClusterNum = freqCentroidsMap[i].length;
			}
		}
		
		corClusterNum = maxClusterNum / 2;
		results = new double[corClusterNum][2];
		
		while(true) {
			if(varNumCents == corClusterNum) {
				break;
			}
			
			Iterator freqIt = freqColMap.entrySet().iterator();
			
			for(int i = 0; i < freqCounter /*Number of frequencies*/; i++) {
				if(varNumCents < freqCentroidsMap[i].length /*Number of centroids*/) {
					
					xArray[i] = freqCentroidsMap[i][varNumCents][showAttrIndex[0]];
					yArray[i] = freqCentroidsMap[i][varNumCents][showAttrIndex[1]];
				}
			}
			
			//Move non-zero elements forward 
			int nonZeroWalker = 0;
			for(int i = 0; i < freqCounter; i++) {
				
				if(xArray[i] != 0) {
					xArray[nonZeroWalker] = xArray[i];
					yArray[nonZeroWalker++] = yArray[i];
				}
			}
			nonZeroWalker--;
			
			double[] nonZeroXarray = Arrays.copyOf(xArray, nonZeroWalker);
			double[] nonZeroYarray = Arrays.copyOf(yArray, nonZeroWalker);
			
			//If only two frequencies has number of corClusterNum clusters, skip correlation calculation,
			//otherwise, do correlation calculation.
			if(nonZeroWalker > 3) {
				results[varNumCents][0] = new PearsonsCorrelation().correlation(nonZeroXarray, nonZeroYarray);
				results[varNumCents][1] = new SpearmansCorrelation().correlation(nonZeroXarray, nonZeroYarray);
			}
			
			varNumCents++;
		}
		
		return results;
	}
	
	/**
	 * Print correlation coefficient value for each frequency
	 */
	public void printCCForFreq(String methodName, double[][] ccResults) {
		for(int i = 0; i < ccResults.length; i++) {
			System.out.print(methodName + "," +"centroid" + i);
			for(int j = 0; j < ccResults[i].length; j++) {
				System.out.format(",%.3f", ccResults[i][j]);
			}
			System.out.println();
		}
	}

}
