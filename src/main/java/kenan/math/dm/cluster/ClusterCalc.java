package kenan.math.dm.cluster;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.DoublePoint;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;

import kenan.demo.FigGen;
import kenan.math.CorrelationDataOffset;
import kenan.math.Counters;
import kenan.math.RawDataOffset;
import kenan.math.StatisticsCSV;
import smile.clustering.XMeans;

public class ClusterCalc extends StatisticsCSV implements RawDataOffset, CorrelationDataOffset {
	
	private final int COR_COL_SIZE = 10;
	private final int CLUSTER_NUM = 9;
	private final int MAX_CLUSTER_NUM = 15;
	//The proportion of cluster size that needs to be considered in calculation
	private final double CLUSTER_PROP = 0.15;
	
	/**Cluster ID that doesn't need to be considered*/
	private boolean[] exclusiveClusterID;
	/**Cluster ID that needs to be considered*/
	private int[] inclusiveClusterID;
	/**Cluster size that needs to be considered*/
	private int[] inclusiveClusterSize;
	/**Clusters that contain all attributes*/
	private double[][][] allAttrCluster;
	/**Map of frequency and its number of rows*/
	protected LinkedHashMap<Integer, Integer> freqColMap = new LinkedHashMap<Integer, Integer>();
	/**Map of frequency Id and it's frequency*/
	protected HashMap<Integer, Integer> freqIDFreqMap = new HashMap<Integer, Integer>();
	/**Map of all points for one method, map from a given attribute to the the rest of attributes of a point*/
	private HashMap<Double, List<List<Double>>> pointsMP = new HashMap<Double, List<List<Double>>>();
	
	/**
	 * Print title
	 * @param showAttr the attributes that need to be generated
	 */
	public void printCentTitle(List<String> list, List<String> showAttr) {
		
		String data = list.get(0);
		String[] columns = data.split(",");
		
		System.out.print("methodName,centroidNo,freqCounter");
		for(String attr : showAttr) {
			if(attr.equals("counter1")) {
				System.out.print("," + columns[COUNTER1_POS]);
			} else if(attr.equals("counter2")) {
				System.out.print("," + columns[COUNTER2_POS]);
			} else if(attr.equals("counterRate")){
				if(hasRateCol) {
					System.out.print("," + columns[columns.length - COUNTER_RATE_NEG]);
				} else {
					System.out.print("," + columns[columns.length - COUNTER_RATE_TIME_NEG]);
				}
			} else {
				System.out.print("," + attr);
			}
		}
		System.out.println();
	}
	
	/**
	 * Add values for each counter list respectively
	 * @param columns list of entries for each row
	 */
	private void addCounterValue(String[] columns) {
		Counters counter = new Counters();
		counter.energy = Double.parseDouble(columns[columns.length - PKG_ENER_NEG]);
		counter.power = Double.parseDouble(columns[columns.length - PKG_POWER_NEG]);
		counter.counter1 = Double.parseDouble(columns[COUNTER1_POS]);
		counter.counterRateByTime = Double.parseDouble(columns[columns.length - COUNTER_RATE_TIME_NEG]);
		counter.time = Double.parseDouble(columns[columns.length - TIME_NEG]);
		counter.freq = Double.parseDouble(columns[FREQUENCY_POS]);
		if(hasRateCol) {
			counter.counter2 = Double.parseDouble(columns[COUNTER2_POS]);
			counter.counterRate = Double.parseDouble(columns[columns.length - COUNTER_RATE_NEG]);
		}
		counters.add(counter);
	}
	
	/**
	 * Get Centroids by variable attributes
	 * @param list list that contains contents of csv file
	 * @param parameter parameters that need to be calculated in clustering
	 * @param showAttr parameters that need to be showed in csv file 
	 * @param showAttr parameters that need to be showed in 3D figures 
	 */
	public void getCentroidsWithVarAttr(List<String> list, List<String> showAttr, List<String> parameter, List<String> figures) {
		int mthMapIndex = 0;
		
		int[] showAttrIndex = new int[showAttr.size()];
		int[] attrsIndex = new int[parameter.size()];
		
		//Get the index for the counters that needs to be calculated
		initArgsIndexMap(attrsIndex, showAttrIndex, parameter, showAttr, hasRateCol);
		
		getMethodMap(list);
		Iterator it = mthInfoMap.entrySet().iterator();
		printCentTitle(list, showAttr);
		while(it.hasNext()) {
			Map.Entry<String, List<String>> pair = (Map.Entry<String, List<String>>)it.next();
			
			//If the count of rows of the method is too small, skip it
			if(!rowCountCheck(mthMapIndex++)) {
				System.out.println("skip the method: " + pair.getKey());
				continue;
			}
			System.out.println("method: " + pair.getKey() + "is considered");
			
			preProcessCentroids(pair, showAttr, parameter, figures, attrsIndex);
			
			//Print centroids
			printCentroids(pair, showAttrIndex);
			
			//Clear variables for each method
			clearVars();
		}
	}
	
	
	public void preProcessCentroids(Map.Entry<String, List<String>> pair, List<String> showAttr, 
			List<String> parameter, List<String> figures, int[] attrsIndex) {
		
		List<String> singleMthList = pair.getValue();
		//Reset variables
		resetVar();
		
		//Record number of cells for each frequency, in order to 
		//discard the frequency with small number of cells
		getFreqRowMap(singleMthList);
		getFreqIdFreqMap();

		//Get centroids for each frequency in order to fairly compare the varied data with 
		//different frequencies
		getCentroids(pair, attrsIndex, parameter, showAttr, figures);
	}
	
	/**
	 * Check if the number of rows of current method need to be considered in clustering
	 * @param mthMapIndex current method id in methInfoMap
	 * @return true if the number of rows of method is bigger than {@code ROW_SIZE_LIMIT},
	 * otherwise return false.
	 */
	public boolean rowCountCheck(int mthMapIndex) {
		List<String> keys = new ArrayList<String>(mthInfoMap.keySet());
		String key = keys.get(mthMapIndex);
		
		if(mthInfoMap.get(key).size() > ROW_SIZE_LIMIT) return true;
		
		return false;
	}
	
	/**
	 * Clear variables for each method
	 */
	public void clearVars() {
		freqCentroidsMap = new Double[StatisticsCSV.FREQ_SIZE][][];
	}
	
	/**
	 * Print centroids
	 * @param pair entry of method to its all data rows
	 * @param showAttrIndex contains the index of attributes need to show
	 */
	public void printCentroids(Map.Entry<String, List<String>> pair, int[] showAttrIndex) {
		
		int maxClusterNum = freqCentroidsMap[0].length;
		
		//Get max number of centroids
		for(int i = 0; i < freqCounter; i++) {
			if(maxClusterNum < freqCentroidsMap[i].length) {
				maxClusterNum = freqCentroidsMap[i].length;
			}
		}
		
		int varNumCents = 0;
		
//		for(int varNumCents = 0; varNumCents < freqCentroidsMap[i].length; varNumCents++) {
		while(true) {
			if(varNumCents == maxClusterNum) {
				break;
			}
			for(int i = 0; i < freqCounter /*Number of frequencies*/; i++) {
				
				if(varNumCents < freqCentroidsMap[i].length /*Number of centroids*/) {
					System.out.print(pair.getKey() + "," + varNumCents + "," + freqIDFreqMap.get(i));
					
					for(int k = 0; k < showAttrIndex.length; k++) {
						System.out.format(",%.3f", freqCentroidsMap[i][varNumCents][showAttrIndex[k]]);
					}
					
					System.out.println();
				}
			}
			varNumCents++;
		}
	}
	
	public void clusterFilter(Map.Entry<String, List<String>> pair, int[] showAttrIndex) {
		int maxClusterNum = freqCentroidsMap[0].length;
		
		while(true) {
			int varNumCents = 0;
			if(varNumCents == maxClusterNum) {
				break;
			}
			Iterator freqIt = freqColMap.entrySet().iterator();
			
			for(int i = 0; i < freqCounter /*Number of frequencies*/; i++) {
				if(varNumCents < freqCentroidsMap[i].length /*Number of centroids*/) {
					Map.Entry<Integer, Integer> freqMap = (Map.Entry<Integer, Integer>)freqIt.next();
					System.out.print(pair.getKey() + "," + varNumCents + "," + freqMap.getKey());
					
					for(int k = 0; k < showAttrIndex.length; k++) {
						System.out.format(",%.3f", freqCentroidsMap[i][varNumCents][showAttrIndex[k]]);
					}
					
					System.out.println();
				}
			}
			varNumCents++;
		}
	}
	
	/**
	 * Using KMeans++ algorithm to calculate centroids of clusters 
	 * @param data one row of attributes information of a method in csv file
	 * @param cells entries contains in data 
	 * @param pair entry maps a method name to its rows
	 * @param attrsIndex index of parameters that need to be calculated in clustering
	 * @param parameter the parameters that need to be calculated in clustering
	 * @param showAttr parameters that need to be showed in csv file 
	 */
	public void getCentroids(Map.Entry<String, List<String>> pair, int[] attrsIndex, List<String> parameter, List<String> showAttr, List<String> figures) {
		
		//list all rows of a method 
		List<String> list = pair.getValue();
		String methodName = pair.getKey(); 
		
		//one row of attributes information of a method in csv file
		String data = list.get(0);
		//entries contains in data 
		String[] cells = data.split(",");
		int prevFreq = Integer.parseInt(cells[FREQUENCY_POS]);
		
		//Accumulate rows until it reaches to the next frequency, do clustering
		for(int i = 0; i < list.size(); i++) {
			data = list.get(i);
			cells = data.split(",");
			int currentFreq = Integer.parseInt(cells[FREQUENCY_POS]);
			//If one frequency only has one column, discard it.
			if(freqColMap.get(currentFreq) <= COR_COL_SIZE) {
				continue;
			} else {
				if(prevFreq == currentFreq) {
					//Add counters for one frequency of a method
					addCounterValue(cells);
					if (i == list.size() - 1) {
						if(!counters.isEmpty()) {
							//Calculate k means centroids
//							freqCentroidsMap[freqCounter++] = kMeansCentroids(attrsIndex, parameter, showAttr);
							//Calculate x means centroids
							freqCentroidsMap[freqCounter++] = xMeansCentroids(attrsIndex, parameter, showAttr, currentFreq, methodName, figures);
							
							//For testing
							if(System.getProperty("test").equalsIgnoreCase("true")) {
								testLoop(freqCentroidsMap[freqCounter - 1], prevFreq);
							}
						} 
					}
				} else {
					if(!counters.isEmpty()) {
						//Calculate k means centroids
//						freqCentroidsMap[freqCounter++] = kMeansCentroids(attrsIndex, parameter, showAttr);
						//Calculate x means centroids
						freqCentroidsMap[freqCounter++] = xMeansCentroids(attrsIndex, parameter, showAttr, prevFreq, methodName, figures);
						//For testing
						if(System.getProperty("test").equalsIgnoreCase("true")) {
							testLoop(freqCentroidsMap[freqCounter - 1], prevFreq);
						}
					}
					
					prevFreq = currentFreq;
					addCounterValue(cells);
				}
			}
		}
		
	}
	
	
	public void clearForNewFreq() {
		counters.clear();
		pointsMP.clear();
	}
	
	/**
	 * Create a map for one frequency to the number of its rows
	 * @param data one row of data
	 * @param columns elements/cells in data
	 * @param list all information of one method
	 */
	public void getFreqRowMap(List<String> list) {
		
		//One row of data
		String data = list.get(0);
		//Elements/cells in data
		String[] columns = data.split(",");
		int prevFreq = Integer.parseInt(columns[FREQUENCY_POS]);
		
		for (int i = 0; i < list.size(); i++) {
			data = list.get(i);
			columns = data.split(",");
			int currentFreq = Integer.parseInt(columns[FREQUENCY_POS]);
			if(prevFreq == currentFreq) {
				colCounter++;
				if (i == list.size() - 1) {
					freqColMap.put(prevFreq, colCounter);
				}
			} else {
				//If one frequency only has one column, discard it.
//				if(colCounter <= COR_COL_SIZE) {
//					colCounter = 1;
//					prevFreq = currentFreq;
//					continue;
//				}
				freqColMap.put(prevFreq, colCounter);
				colCounter = 1;
				prevFreq = currentFreq;
			}
		}
	}
	
	public void getFreqIdFreqMap() {
		int i = 0;
		for(Map.Entry<Integer, Integer> map : freqColMap.entrySet()) {
			freqIDFreqMap.put(i++, map.getKey());
		}
	}
	
	/**
	 * Reset variables
	 */
	public void resetVar() {
		colCounter = 0;
		freqCounter = 0;
		counters.clear();
		freqColMap.clear();
	}
	
	/**
	 * Test centroids 
	 * @param list
	 * @param frequency
	 */
	public void testLoop(Double[][] list, int frequency) {
		for(int i = 0; i < list.length; i++) {
			System.out.print(i);
			for(int j = 0; j < list[i].length; j++) {
				System.out.print("," + list[i][j]);
			}
			System.out.println();
		}
	}
	
	/**
	 * Add point to the map of points that associates the attribute is calculated in K-means 
	 * with other attributes that are not considered but belong to the same point 
	 * Note: Using one attribute as key could have 
	 * potential problem that two points are belong to two cluster but have the same value. However
	 * this possibility is super small concerns the precision of attributes. And most likely, these points 
	 * would be in the same cluster especially when not many attributes are considered
	 * @param key the attribute is calculated in K-means 
	 * @param point all attributes of the point
	 */
	public void addPointsMP(double key, List<Double> point) {
		if(pointsMP.containsKey(key)) {
			pointsMP.get(key).add(point);
		} else {
			List<List<Double>> points = new ArrayList<List<Double>>();
			points.add(point);
			pointsMP.put(key, points);
		}
	}
	
	/**
	 * Using KMeans++ algorithm to calculate centroids of clusters
	 * @return centroids for one frequency
	 */
	public Double[][] kMeansCentroids(int[] attrsIndex, List<String> parameter, List<String> showAttr) {
		List<DoublePoint> points = new ArrayList<DoublePoint>();
		//Initialize clusterer
		KMeansPlusPlusClusterer clusterer = new KMeansPlusPlusClusterer(CLUSTER_NUM);
		int attrNum = hasRateCol ? NUM_WITH_RATE : NUM_WITHOUT_RATE;
		//Loop unswitching, a bit boilerplate, but it could be performance killer if put "if" statement in the loop
		if(parameter.get(0).equals("--all") || parameter.get(0).equals("-a")) {
			for(Counters counter : counters) {
				double[] groupedPoint = new double[attrNum] ;
				groupedPoint[ENERGY] = counter.energy;
				groupedPoint[POWER] = counter.power;
				groupedPoint[COUNTER_RATE_BY_TIME] = counter.counterRateByTime;
				groupedPoint[TIME] = counter.time;
				groupedPoint[COUNTER1] = counter.counter1;
				groupedPoint[FREQUENCY] = counter.freq;
				if(hasRateCol) {
					groupedPoint[COUNTER2] = counter.counter2;
					groupedPoint[COUNTER_RATE] = counter.counterRate;
				}
				points.add(new DoublePoint(groupedPoint));
			}
		} else {
			for(Counters counter : counters) {
				Double[] groupedPoint = new Double[attrNum];
				groupedPoint[ENERGY] = counter.energy;
				groupedPoint[POWER] = counter.power;
				groupedPoint[COUNTER_RATE_BY_TIME] = counter.counterRateByTime;
				groupedPoint[TIME] = counter.time;
				groupedPoint[COUNTER1] = counter.counter1;
				groupedPoint[FREQUENCY] = counter.freq;
				if(hasRateCol) {
					groupedPoint[COUNTER2] = counter.counter2;
					groupedPoint[COUNTER_RATE] = counter.counterRate;
				}
				double[] attrPoint = new double[parameter.size()];
				for(int i = 0; i < parameter.size(); i++) {
					attrPoint[i] = groupedPoint[attrsIndex[i]];
				}
				points.add(new DoublePoint(attrPoint));
				addPointsMP(attrPoint[0], Arrays.asList(groupedPoint));
			}
		}
		//Do cluster for each frequency
		int clusterNum = 0;
		Double[][] centroids = new Double[CLUSTER_NUM][attrNum];
		//Annoying initialization for boxed type Double. Can't use primitive types 
		//because of sorting for centroids. Annoying Java genetics again...
		for(int i = 0; i < CLUSTER_NUM; i++) {
			for(int j = 0; j < attrNum; j++) {
				centroids[i][j] = 0.0d;
			}
		}
		List<CentroidCluster> cents = clusterer.cluster(points);
		for(CentroidCluster cluster : cents) {
//			centroids[i++] = centroid.getCenter().getPoint();
			List<DoublePoint> clusteredPoints = (ArrayList<DoublePoint>)cluster.getPoints();
			for(DoublePoint point : clusteredPoints) {
				double[] key = point.getPoint();
				//Only when key is not unique, in majority cases it would be only 1 loop
				for(List<Double> repetitives : pointsMP.get(key[0])) {
//					if(attrNum != repetitives.size()) {
//						System.err.println("Number of attributes and number of attributes for a point is different!");
//					}
					//Accumulate values
					for(int j = 0; j < attrNum; j++) {
						centroids[clusterNum][j] += repetitives.get(j);
					}
				}
			}
			//Get euclidean mean
			for(int j = 0; j < attrNum; j++) {
				centroids[clusterNum][j] /= clusteredPoints.size();
			}
			clusterNum++;
		}

		//TODO: add an argument for sorting later
		if(!parameter.get(0).equals("all")) {
			sortCentByGivenCounter(centroids, counterIndexMap.get(parameter.get(0)));
		} else {
			sortCentByGivenCounter(centroids, counterIndexMap.get(showAttr.get(0)));
		}

		return centroids;
	}
	
	/**
	 * Group attributes need to be calculated in clustering based on command line input.
	 * If only partial attributes are counted in clustering, then create a map to associate 
	 * other attributes that are not considered in clustering
	 * @param parameter
	 * @param attrNum
	 * @param points
	 * @param attrsIndex
	 */
	public void groupAttrPoint(List<String> parameter, int attrNum, double[][] points, int[] attrsIndex) {
		int pointID = 0;
		//Loop unswitching, a bit boilerplate, but it could be performance killer if put "if" statement in the loop
		if(parameter.get(0).equals("--all") || parameter.get(0).equals("-a")) {
			for(Counters counter : counters) {
				double[] groupedPoint = new double[attrNum] ;
				groupedPoint[ENERGY] = counter.energy;
				groupedPoint[POWER] = counter.power;
				groupedPoint[COUNTER_RATE_BY_TIME] = counter.counterRateByTime;
				groupedPoint[TIME] = counter.time;
				groupedPoint[COUNTER1] = counter.counter1;
				groupedPoint[FREQUENCY] = counter.freq;
				if(hasRateCol) {
					groupedPoint[COUNTER2] = counter.counter2;
					groupedPoint[COUNTER_RATE] = counter.counterRate;
				}
				//Gather data of points 
				points[pointID++] = groupedPoint;
				
			}
		} else {
			for(Counters counter : counters) {
				Double[] groupedPoint = new Double[attrNum];
				groupedPoint[ENERGY] = counter.energy;
				groupedPoint[POWER] = counter.power;
				groupedPoint[COUNTER_RATE_BY_TIME] = counter.counterRateByTime;
				groupedPoint[TIME] = counter.time;
				groupedPoint[COUNTER1] = counter.counter1;
				groupedPoint[FREQUENCY] = counter.freq;
				if(hasRateCol) {
					groupedPoint[COUNTER2] = counter.counter2;
					groupedPoint[COUNTER_RATE] = counter.counterRate;
				}
				double[] attrPoint = new double[parameter.size()];
				for(int i = 0; i < parameter.size(); i++) {
					attrPoint[i] = groupedPoint[attrsIndex[i]];
				}
				//Gather data of points 
				points[pointID++] = attrPoint;
				addPointsMP(attrPoint[0], Arrays.asList(groupedPoint));
				
			}
		}
	}
	
	/**
	 * Construct centroids that contain all attributes
	 * @param centroids
	 * @param clusterSize
	 * @param atcounterstrNum
	 */
	public void constructAllAttrCentroids(Double[][] centroids, int attrNum) {
        for(int i = 0; i < allAttrCluster.length; i++) {
        	for(int j = 0; j < allAttrCluster[i].length; j++) {
        		for(int k = 0; k < attrNum; k++) {
        			centroids[i][k] += allAttrCluster[i][j][k];
        		}
        	}
        	
        	//Get euclidean mean
        	for(int k = 0; k < attrNum; k++) {
        		centroids[i][k] /= allAttrCluster[i].length;
        	}
        }
	}
	
	/**
	 * Construct cluster that contain all attributes
	 * @param clusters clusters only contain one attribute
	 * @param attrNum
	 */
	public void constructAllAttrCluster(double[][][] clusters, int attrNum) {
        
        HashMap<Double, Boolean> isCalculated = new HashMap<Double, Boolean>();
        int clusterId = 0;
        int pointId = 0;

        //Initialize size of allAttrCluster
        {
        	int i = 0;
        	allAttrCluster = new double[clusters.length][][];
        	
	        for(double[][] cluster : clusters) {
	        	allAttrCluster[i++] = new double[cluster.length][attrNum];
	        }
        }
        
		for(double[][] pointsWithinCluster : clusters) {
			pointId = 0;
			for(double[] key: pointsWithinCluster) {
    			//For the most cases, the key only maps to one value in the map. In some seldom cases, 
    			//the points share the same attribute value, it doesn't matter when we only use one 
    			//attribute to do clustering since they share the same value they must belong to the same cluster.
				//Loop unswitching
    			//TODO: change this issue later
				if(pointsMP.get(key[0]).size() > 1 && (!isCalculated.containsKey(key[0]) || !isCalculated.get(key[0]))) {
					for(List<Double> duplicates : pointsMP.get(key[0])) {
	
						//Accumulate values
						for(int j = 0; j < attrNum; j++) {
							allAttrCluster[clusterId][pointId][j] = duplicates.get(j);
						}
						pointId++;
					}
					isCalculated.put(key[0], true);
					
				} else if(pointsMP.get(key[0]).size() == 1) {
					List<Double> single = pointsMP.get(key[0]).get(0);
					for(int j = 0; j < attrNum; j++) {
						allAttrCluster[clusterId][pointId][j] = single.get(j);
					}
					pointId++;
				} 
			}

			clusterId++;
        }
	}
	
	/**
	 * Using XMeans algorithm to calculate centroids of clusters
	 * @param attrsIndex index of parameters that need to be calculated in clustering
	 * @param parameter the parameters that need to be calculated in clustering
	 * @param showAttr parameters that need to be showed in csv file 
	 * @param currentFreq the data of the Frequency is processed
	 * @param methodName the name of method that is processed
	 * @param showAttr parameters that need to be showed in 3D figures
	 * @return centroids for one frequency
	 */
	public Double[][] xMeansCentroids(int[] attrsIndex, List<String> parameter, List<String> showAttr, 
			int currentFreq, String methodName, List<String> figures) {
		
		double[][] points = new double[counters.size()][];
		int attrNum = hasRateCol ? NUM_WITH_RATE : NUM_WITHOUT_RATE;

		//Grouped attributes for XMeans
		groupAttrPoint(parameter, attrNum, points, attrsIndex);
		
		//Initialize X-Means
		XMeans xmeans = new XMeans(points, MAX_CLUSTER_NUM);
		
		int numClusters = xmeans.getNumClusters();
		int[] originalClusterSize = xmeans.getClusterSize();
		//Number of clusters that deserve to be considered in our calculation
		int numClusterConsider = 0;
		
		exclusiveClusterID = new boolean[numClusters];
		inclusiveClusterID = new int[numClusters];
		
		for(int i = 0; i < numClusters; i++) {
//			System.out.println("originalClusterSize: " + originalClusterSize[i] + " points length is: " + points.length);
			if((double)originalClusterSize[i] / (double)points.length > CLUSTER_PROP) {
				numClusterConsider++;
			} else {
				exclusiveClusterID[i] = true; 
			}
		}
		
		inclusiveClusterSize = new int[numClusterConsider];
		double[][][] clusters = new double[numClusterConsider][][];
		int clusterID = 0;
		for (int i = 0; i < numClusters; i++) {
			if(!exclusiveClusterID[i]) {
				clusters[clusterID] = new double[originalClusterSize[i]][];
				inclusiveClusterID[i] = clusterID;
				inclusiveClusterSize[clusterID] = originalClusterSize[i];
				clusterID++;
			}
		}
		
		//Initialize centroids
		Double[][] centroids = new Double[numClusterConsider][attrNum];
		//Annoying initialization for boxed type Double. Can't use primitive types 
		//because of sorting for centroids. Annoying Java genetics...
		for(int i = 0; i < numClusterConsider; i++) {
			for(int j = 0; j < attrNum; j++) {
				centroids[i][j] = 0.0d;
			}
		}
		
        //Point ID within one cluster
        int[] localPointID = new int[numClusters];
        //Point ID for whole data set
        int globalPointID;
        
        //Assign points to clusters
        for (globalPointID = 0; globalPointID < points.length; globalPointID++) {
        	clusterID = xmeans.getClusterLabel()[globalPointID];
        	//Handle reduced clusters
        	if(!exclusiveClusterID[clusterID]) {
        		clusters[inclusiveClusterID[clusterID]][localPointID[clusterID]++] = points[globalPointID];
        	}
        }

        //Create clusters contain all attributes (note the position of points are not changed,
        //just associate with all attributes of the points in cluster
        constructAllAttrCluster(clusters, attrNum);
        
        //Associate the attribute that is calculated by xmeans above to other attributes 
        //that are belongs to the same point
        constructAllAttrCentroids(centroids, attrNum);
        
		//Draw figures
        if(System.getProperty("figure").equalsIgnoreCase("true")) {
			FigGen figGen = new FigGen(clusters, xmeans, allAttrCluster, pointsMP, counters.size(), 
					points, centroids, counterIndexMap, figures);
			figGen.generateFig(parameter.get(0), currentFreq, methodName);
        }
		
		//TODO: add an argument for sorting later
		if(!parameter.get(0).equals("all")) {
			sortCentByGivenCounter(centroids, counterIndexMap.get(parameter.get(0)));
		} else {
			sortCentByGivenCounter(centroids, counterIndexMap.get(showAttr.get(0)));
		}
		
		//Reset for the new frequency
		clearForNewFreq();
		
		return centroids;
	}
	
	public void sortCentByGivenCounter(Double[][] centroids, final int sortColIndex) {
		final Comparator<Double[]> arrayComparator = new Comparator<Double[]>() {
			@Override
			public int compare(Double[] o1, Double[] o2) {
				return o1[sortColIndex].compareTo(o2[sortColIndex]);
			}
		};
		Arrays.sort(centroids, arrayComparator);
	}
}

