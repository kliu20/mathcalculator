package kenan.math;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class StatisticsCSV implements RawDataOffset, CorrelationDataOffset {

	protected static int freqCounter = 0;
	protected boolean hasRateCol = false; 
	/**Column counter for each frequency*/
	protected static int colCounter = 0;
	/**Row counter for each frequency*/
	protected int rowCounter = 0;
	/**Frequency size*/
	protected static final int FREQ_SIZE = 8;
	/**Number of rows limit be considered in clustering*/
	protected static final int ROW_SIZE_LIMIT = 30;
	/**Method to energy/hardware counter information*/
	protected final LinkedHashMap<String, List<String>> mthInfoMap = new LinkedHashMap<String, List<String>>();
	/**Counter string to counter index map*/
	protected final HashMap<String, Integer> counterIndexMap = new HashMap<String, Integer>();
	/**Attributes of centroids for each frequency, number of frequencies is AT MOST FREQ_SIZE*/
	protected static Double[][][] freqCentroidsMap = new Double[StatisticsCSV.FREQ_SIZE][][];
	/**List of one frequency rows of a method, each Counters object is a row contains hardware counters in the csv file*/
	protected List<Counters> counters = new ArrayList<Counters>();
	/**Counter1 name*/
	protected String counterName1 = null;
	/**Counter2 name*/
	protected String counterName2 = null;

	
	
	public void setHasRateCol(List<String> list) {
		String[] columns = list.get(0).split(",");
		if(columns.length == HAS_RATE_NUM) {
			hasRateCol = true;
		}
	}
	
	/**
	 * @param fileName
	 * @param list list that contains the contents of a file
	 */
	public void extractValue(String fileName, List<String> list) {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
			String preProc = null;
			while((preProc = input.readLine()) != null) {
				list.add(preProc);
			}
			if(list.get(0).length() == 0) {
				return;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(IOException e) {
			
		}
	}
	
	/**
	 * Initialize counter names to counter index map (ugly)
	 */
	public void initCounterIndexMap() {
		counterIndexMap.put("energy", ENERGY);
		counterIndexMap.put("power", POWER);
		counterIndexMap.put("counter_rate_by_time", COUNTER_RATE_BY_TIME);
		counterIndexMap.put("time", TIME);
		counterIndexMap.put("frequency", FREQUENCY);
		counterIndexMap.put("counter1", COUNTER1);
		counterIndexMap.put("counter2", COUNTER2);
	}
	
	/**
	 * Initialize counter names of arguments that need to be calculated for K-Means
	 * @param attrsIndex index of parameters to be calculated in K-means
	 * @param showAttrIndex index of attributes to be printed out
	 * @param parameter parameters that need to be calculated in K-means
	 * @param showAttr attributes that need to be printed out
	 * @param hasRateCol does csv file contain rate column?
	 */
	public void initArgsIndexMap(int[] attrsIndex, int[] showAttrIndex, List<String> parameter, List<String> showAttr, boolean hasRateCol) {
		int i = 0;
		//Insert appropriate rate index into map
		addCounterIndexMap("counterRate", hasRateCol ? COUNTER_RATE : COUNTER_RATE_BY_TIME);
		//If specify 2 as number of dimensions that need to be calculated but only one attribute is provided,
		//using default rate as input
		if(!parameter.get(0).equals("all")) {
			for(String param : parameter) {
				if(param.equals("counterRate")) {
					attrsIndex[i++] = counterIndexMap.get("counterRate");
				} else {
					attrsIndex[i++] = counterIndexMap.get(param);
				}
			}
		}
		i = 0;
		for(String show : showAttr) {
			showAttrIndex[i++] = counterIndexMap.get(show);
		}
	}
	
	/**
	 * Add counter name to its index
	 * @param key counter name
	 * @param value index of the array
	 */
	public void addCounterIndexMap(String key, int value) {
		counterIndexMap.put(key, value);
	}
	
	/**
	 * Get method name and energy/hardware counters information map
	 * @param list contents of csv file
	 */
	public void getMethodMap(List<String> list) {
		String data;
		String[] columns;
		for(int i = 1; i < list.size(); i++) {
			data = list.get(i);
			columns = data.split(",");
			if(!mthInfoMap.containsKey(columns[METHODNAME_POS])) {
				List<String> newList = new ArrayList<String>();
				newList.add(data);
				mthInfoMap.put(columns[METHODNAME_POS], newList);
			} else {
				mthInfoMap.get(columns[METHODNAME_POS]).add(data);
			}
		}
	}
	
	/**
	 * @param list
	 * @return double array
	 */
	public double[] ListToArray(List<Double> list) {
		double[] array = new double[list.size()];
		int i = 0;
		for(Double element : list) {
			array[i++] = element;
		}
		return array;
	}
	
	/**
	 * Extract hardware counter names for each csv file input
	 * @param list
	 */
	public void extractCounterNames(List<String> list) {
		String data = list.get(0);
		String[] columns = data.split(",");
		counterName1 = columns[COUNTER1_POS];
		if(columns.length == 15) {
			counterName2 = columns[COUNTER2_POS];
		} else if(columns.length == 14) {
			counterName2 = "None";
		}
	}
	
	/**
	 * Print Title
	 */
	public void printTitle(){
		System.out.println("methodName,frequency,energy,power," + counterName1 
					+ "," + counterName2 + ",counterRate,counterRateByTime,time");
	}
}
