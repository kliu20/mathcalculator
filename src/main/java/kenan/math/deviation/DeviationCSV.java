package kenan.math.deviation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import kenan.math.RawDataOffset;
import kenan.math.StatisticsCSV;

import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

public class DeviationCSV extends StatisticsCSV implements RawDataOffset {
	final int FREQ_SIZE = 8;
	List<Double> counter1 = new ArrayList<Double>();
	List<Double> counter2 = new ArrayList<Double>();
	List<Double> energy = new ArrayList<Double>();
	List<Double> power = new ArrayList<Double>();
	List<Double> counterRate = new ArrayList<Double>();
	List<Double> counterRateByTime = new ArrayList<Double>();
	List<Double> time = new ArrayList<Double>();
	StandardDeviation sd = new StandardDeviation();

	/**
	 * Calculate stand deviation and print out
	 * 
	 * @param list
	 */
	public void getSD(List<String> list) {
		String data;
		String[] columns;

		// Better solution, but more complicated, don't wanna use it for
		// this simple program.
		// Record number of columns for each frequency
		// for(int i = 1; i < list.size(); i++) {
		// data = list.get(i);
		// columns = data.split(",");
		// if(prevFreq == Integer.parseInt(columns[0])) {
		// colCounter++;
		// prevFreq = Integer.parseInt(columns[0]);
		// if(i == list.size() - 1) {
		// freqColumns[i-1] = colCounter;
		// }
		// } else {
		// //If one frequency only has one column, discard it.
		// if(colCounter == 1) {
		// prevFreq = Integer.parseInt(columns[0]);
		// continue;
		// }
		// freqColumns[i-1] = colCounter;
		// colCounter = 1;
		// prevFreq = Integer.parseInt(columns[0]);
		// }
		// }
		
		getMethodMap(list);
		
		Iterator it = mthInfoMap.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, List<String>> pair = (Map.Entry<String, List<String>>)it.next();
			List<String> singleMthList = pair.getValue();
			//Reset variables
			data = singleMthList.get(0);
			columns = data.split(",");
			int prevFreq = Integer.parseInt(columns[0]);
			resetVar();
			
			for (int i = 0; i < singleMthList.size(); i++) {
				data = singleMthList.get(i);
				columns = data.split(",");
				if (prevFreq == Integer.parseInt(columns[0])) {
					colCounter++;
					prevFreq = Integer.parseInt(columns[0]);
					addCounterValue(columns);
					if (i == singleMthList.size() - 1) {
						printSDForFreq(pair.getKey(), prevFreq);
					}
				} else {
	
					// If one frequency only has one column, discard it.
					if (colCounter == 1) {
						prevFreq = Integer.parseInt(columns[0]);
						continue;
					}
					// Print mean value
					printSDForFreq(pair.getKey(), prevFreq);
					// Move to the next frequency
					colCounter = 1;
	
					prevFreq = Integer.parseInt(columns[0]);
					// Reset for the new frequency
					energy = new ArrayList<Double>();
					power = new ArrayList<Double>();
					counter1 = new ArrayList<Double>();
					counterRateByTime = new ArrayList<Double>();
					counter2 = new ArrayList<Double>();
					counterRate = new ArrayList<Double>();
					time = new ArrayList<Double>();
					addCounterValue(columns);
				}
			}
		}
	}

	/**
	 * Reset variables
	 */
	public void resetVar() {
		counter1 = new ArrayList<Double>();
		counter2 = new ArrayList<Double>();
		energy = new ArrayList<Double>();
		power = new ArrayList<Double>();
		counterRate = new ArrayList<Double>();
		counterRateByTime = new ArrayList<Double>();
		time = new ArrayList<Double>();
	}
	
	/**
	 * Print standard deviation value for each frequency
	 */
	public void printSDForFreq(String methodName, int freq) {
//		for(double p : power) {
//			System.out.println(methodName + ": " + p);
//		}
		System.out.print(methodName + "," + freq + "," + sd.evaluate(ListToArray(energy)) + ","
				+ sd.evaluate(ListToArray(power)) + ","
				+ sd.evaluate(ListToArray(counter1)) + ","
				+ sd.evaluate(ListToArray(counter2)) + ","
				+ sd.evaluate(ListToArray(counterRate)) + ","
				+ sd.evaluate(ListToArray(counterRateByTime)) + ","
				+ sd.evaluate(ListToArray(time)));
		System.out.println();
	}

	/**
	 * Add values for each counter list respectively
	 * 
	 * @param columns
	 *            String list for each row
	 */
	private void addCounterValue(String[] columns) {
		energy.add(Double.parseDouble(columns[columns.length - PKG_ENER_NEG]));
		power.add(Double.parseDouble(columns[columns.length - PKG_POWER_NEG]));
		counter1.add(Double.parseDouble(columns[COUNTER1_POS]));
		counterRateByTime.add(Double.parseDouble(columns[columns.length
				- COUNTER_RATE_TIME_NEG]));
		time.add(Double.parseDouble(columns[columns.length - TIME_NEG]));
		if (columns.length == HAS_RATE_NUM) {
			counter2.add(Double.parseDouble(columns[COUNTER2_POS]));
			counterRate.add(Double.parseDouble(columns[columns.length
					- COUNTER_RATE_NEG]));
		}
	}
}
