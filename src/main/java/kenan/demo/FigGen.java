package kenan.demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import smile.clustering.XMeans;
import smile.plot.Palette;
import smile.plot.PlotCanvas;
import smile.plot.ScatterPlot;

/**
 * This class is basically mimic the implementation of 
 * classes in smile.demo.clustering 
 */
public class FigGen extends JPanel implements Runnable, ActionListener, AncestorListener {
	private char pointLegend = '@';
	private double[][][] allAttrCluster;
	private double[][][] clusters;
	private double[][] rawPoints;
	/**Point contains the attributes need to be showed*/
	private double[][][] plotClusters;
	private double[][] Centroids3D;
	private int clusterNumber = 0;
	
	private static final int PLOT3D = 3;
	private static final int PLOT2D = 2;
	
	private XMeans xmeans;
//	private JComponent canvas;
	private PlotCanvas canvas;
    private JTextField clusterNumberField;
    private JButton startButton;
    
    private HashMap<Double, Boolean> isCalculated = new HashMap<Double, Boolean>();
	
    JTextField maxClusterNumberField;
    int maxClusterNumber = 15;
    JPanel optionPane;
    JPanel titlePane;
	
	public FigGen(double[][][] clusters, XMeans xmeans, double[][][] allAttrCluster, 
			HashMap<Double, List<List<Double>>> pointsMP, int plotPointSize, 
			double[][] rawPoints, Double[][] centroids, HashMap<String, Integer> counterIndexMap, List<String> figures) {
		
		if(figures.size() != 3) {
        	System.err.println("Only support 3D figure generation!");
        	System.exit(-1);
        }
		
		this.xmeans = xmeans;
		this.clusters = clusters;
		this.allAttrCluster = allAttrCluster;
		this.rawPoints = rawPoints;
		this.Centroids3D = plotCentInit(centroids, counterIndexMap, figures);
		
		int[] centClusterId = new int[centroids.length];
		Color[] colors = new Color[centroids.length];
		//Initialize labels for centroids plot
		initCentClusterId(centClusterId);
		//Initialize colors for centroids plot
		initCentColor(colors);
		
		//Point contains the attributes need to be showed
//		plotClusters = new double[xmeans.getNumClusters()][][];
		plotClusters = new double[clusters.length][][];
		
//		initPlotClusters(xmeans.getClusterSize());
		initPlotClusters(clusters);

		
		//Reinitialize clusters with more attributes to plot
		plotPointsInit(allAttrCluster, counterIndexMap, figures);
		
        addAncestorListener(this);

        startButton = new JButton("Start");
        startButton.setActionCommand("startButton");
        startButton.addActionListener(this);
        clusterNumberField = new JTextField("2", 5);
		
        optionPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        optionPane.setBorder(BorderFactory.createRaisedBevelBorder());
        optionPane.add(startButton);
        optionPane.add(new JLabel("K:"));
        optionPane.add(clusterNumberField);

        setLayout(new BorderLayout());
        add(optionPane, BorderLayout.NORTH);

        canvas = ScatterPlot.plot(Centroids3D, centClusterId, '*', colors);
        for (int k = 0; k < clusters.length; k++) {
        	canvas.points(plotClusters[k], pointLegend, Palette.COLORS[k % Palette.COLORS.length]);
        }
    	canvas.setAxisLabels(figures.get(0), figures.get(1), figures.get(2));
        add(canvas, BorderLayout.CENTER);
		
        maxClusterNumberField = new JTextField(Integer.toString(maxClusterNumber), 5);
        optionPane.add(new JLabel("Max K:"));
        optionPane.add(maxClusterNumberField);
	}
	
	public void initPlotClusters(double[][][] clusters) {
		for(int i = 0; i < plotClusters.length; i++) {
			plotClusters[i] = new double[clusters[i].length][PLOT3D];
		}
	}
	
	public void initCentColor(Color[] colors) {
		for(int i = 0; i < colors.length; i++) {
			colors[i] = Palette.COLORS[i % Palette.COLORS.length];
		}
	}
	
	public void initCentClusterId(int[] centClusterId) {
		for(int i = 0; i < centClusterId.length; i++) {
			centClusterId[i] = i;
		}
	}
	
	/**
	 * Unbox centroids and pick 2 or 3 (depends on 2D or 3D figures to plot) attributes to plot
	 * @param centroids
	 * @return
	 */
	public double[][] plotCentInit(Double[][] centroids, HashMap<String, Integer> counterIndexMap, List<String> figures) {
		double[][] unboxCentroids = new double[centroids.length][PLOT3D];
//		System.out.println("number of centroids is: " + centroids.length);
		for(int i = 0; i < centroids.length; i++) {
			unboxCentroids[i][0] = centroids[i][counterIndexMap.get(figures.get(0))];
			unboxCentroids[i][1] = centroids[i][counterIndexMap.get(figures.get(1))];
			unboxCentroids[i][2] = centroids[i][counterIndexMap.get(figures.get(2))];
		}
		return unboxCentroids;
	}
	
	/**
	 * Recreate clusters by picking up partial attributes to be plotted 
	 * @param allAttrCluster points of clusters contain all attributes
	 * @param paramIndex index of the parameter calculated in clustering, that 
	 * means this parameter must included in {@code plotClusters} 
	 */
	public void plotPointsInit(double[][][] allAttrCluster, HashMap<String, Integer> counterIndexMap, List<String> figures) {
        for(int i = 0; i < allAttrCluster.length; i++) {
        	for(int j = 0; j < allAttrCluster[i].length; j++) {
        		plotClusters[i][j][0] = allAttrCluster[i][j][counterIndexMap.get(figures.get(0))];
        		plotClusters[i][j][1] = allAttrCluster[i][j][counterIndexMap.get(figures.get(1))];
        		plotClusters[i][j][2] = allAttrCluster[i][j][counterIndexMap.get(figures.get(2))];
        	}
        }
	}
	
    @Override
    public void actionPerformed(ActionEvent e) {

        if ("startButton".equals(e.getActionCommand())) {
            try {
                clusterNumber = Integer.parseInt(clusterNumberField.getText().trim());
                if (clusterNumber < 2) {
                    JOptionPane.showMessageDialog(this, "Invalid K: " + clusterNumber, "error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (clusterNumber > plotClusters.length / 2) {
                    JOptionPane.showMessageDialog(this, "Too large K: " + clusterNumber, "error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Invalid K: " + clusterNumberField.getText(), "error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            Thread thread = new Thread(this);
            thread.start();
        } 
    }
	
    /**
     * Recalculate when click start button
     */
	public PlotCanvas getPlotCanvas() {
		XMeans xmeans = new XMeans(rawPoints, clusterNumber);
		System.out.println("xmeans in getPLotCanvas: " + xmeans.getNumClusters());
		//TODO: Only has one dimension here. Needs to associate with other attributes as it is in correlationCSV
        PlotCanvas canvas = ScatterPlot.plot(xmeans.centroids(), '@');
        
        for (int k = 0; k < xmeans.getNumClusters(); k++) {
        	canvas.points(plotClusters[k], pointLegend, Palette.COLORS[k % Palette.COLORS.length]);
        }
        
        canvas.points(xmeans.centroids(), '@');
        return canvas;
	}
	
	@Override
    public void run() {
        startButton.setEnabled(false);
//        datasetBox.setEnabled(false);

        try {
        	PlotCanvas plot = getPlotCanvas();
        	if (plot != null) {
        		remove(canvas);
        		canvas = plot;
        		add(canvas, BorderLayout.CENTER);
        	}
        	validate();
        } catch (Exception ex) {
        	System.err.println(ex);
        }

        startButton.setEnabled(true);
//        datasetBox.setEnabled(true);
    }
	
	public void generateFig(String param, int currentFreq, String methodName) {
        JFrame f = new JFrame("X-Means: " + methodName + " for frequency: " + currentFreq);
        f.setSize(new Dimension(1000, 1000));
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(this);
        f.setVisible(true);
	}

	@Override
	public void ancestorAdded(AncestorEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ancestorRemoved(AncestorEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ancestorMoved(AncestorEvent event) {
		// TODO Auto-generated method stub
		
	}

}
