package kenan;
import java.util.ArrayList;
import java.util.List;

import kenan.math.StatisticsCSV;
import kenan.math.dm.cluster.ClusterCalc;
import kenan.math.dm.correlation.CorrelationCSV;
import kenan.math.deviation.DeviationCSV;
import kenan.math.mean.MeanCSV;


public class Harness {
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		//Parameters that need to be calculated in K-means
		List<String> parameter = new ArrayList<String>();
		//Attributes that need to be generated 
		List<String> showAttr = new ArrayList<String>();
		//Attrbutes that need to be showed in 3D figures
		List<String> figures = new ArrayList<String>();
		
		switch(args[0]) {
			//Generate mean values for data in csv file
			case "mean":
				MeanCSV mean = new MeanCSV();
				argumentsHandler(mean, args, null, null, list, null);
				mean.extractCounterNames(list);
				mean.printTitle();
				mean.getMean(list);
				break;
		
			//Generate standard deviation for for data in csv file
			case "sd":
				DeviationCSV sd = new DeviationCSV();
				argumentsHandler(sd, args, null, null, list, null);
				sd.extractCounterNames(list);
				sd.printTitle();
				sd.getSD(list);
				break;
				
			//Generate correlation coefficient for data in csv file
			case "correlation":
				CorrelationCSV cor = new CorrelationCSV();
				cor.initCounterIndexMap();
				argumentsHandler(cor, args, parameter, showAttr, list, figures);
				cor.extractCounterNames(list);
				cor.setHasRateCol(list);
				cor.getCorrelation(list, showAttr, parameter, figures);
				break;
				
			//Generate centroids for data in csv file
			case "centroid":
				ClusterCalc cluster = new ClusterCalc();
				cluster.initCounterIndexMap();
				argumentsHandler(cluster, args, parameter, showAttr, list, figures);
				cluster.extractCounterNames(list);
				cluster.setHasRateCol(list);
				cluster.getCentroidsWithVarAttr(list, showAttr, parameter, figures);
				break;
			default:
				break;
		}
	}
	
	/**
	 * Handle the arguments and initialize structures
	 * @param cor
	 * @param args
	 * @param parameter
	 * @param showAttr
	 * @param list
	 */
	public static void argumentsHandler(StatisticsCSV math, String[] args, 
			List<String> parameter, List<String> showAttr, List<String> list, List<String> figures) {
		for(int i = 1; i < args.length; i++) {
			if(args[i].equals("-i") || args[i].equals("--input")) {
				math.extractValue(args[++i], list);
				
			} else if(args[i].equals("-p") || args[i].equals("--parameter")) {
				while(++i < args.length && args[i].charAt(0) != '-') {
					parameter.add(args[i]);
				}
				i--;
				
			} else if(args[i].equals("-s") || args[i].equals("--show")) {
				while(++i < args.length && args[i].charAt(0) != '-') {
					showAttr.add(args[i]);
				}
				i--;
				
			} else if(args[i].equals("-a") || args[i].equals("--all")) {
				parameter.add(0, "all");
				
			} else if(args[i].equals("-f") || args[i].equals("--figure")) {
				while(++i < args.length && args[i].charAt(0) != '-') {
					figures.add(args[i]);
				}
				i--;
				
			} else {
				errorMessageForCent("centroid");
			}
		}
	}
	
	public static void errorMessageForCent(String algName) {
		System.out.println("Usage:" + algName + " [Options] -i file");
		System.out.println("		-i, --input <file> the file to be processed");
		System.out.println("		-p, --parameter <args> parameters that need to be calculated in K-means");
		System.out.println("		-s, --show <args> parameters that need to be showed in csv file");
		System.out.println("		-f, --figure <args> parameters that need to be showed in csv file");
		System.out.println("		-a, --all take all parameters into consideration for K-means calculation");
		System.exit(0);
	}
}
