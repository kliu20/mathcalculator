import sys
import pandas as pd
import os
import getopt
import matplotlib.pyplot as plt
import numpy as np
from os import walk

#Number of centroids passing from command argument
numCent = ''
#Input directory passing from command argument
inputDir = ''

dataFrame = pd.DataFrame()


#Current python working directory path
def path():
	cwd = os.getcwd()
	return cwd + '/'

#def genCol():
#	#cols = ["" for str(col) in range(numCent)]
#	for col in range(numCent):
#		"".
#	print cols
#	return cols

#Add file names
def getDataFrame(path):
	global numCent
	files = []
	#Get sub directories list. It's actually two loops instead
	#of a nested loop. First Python program. Is there a better way?
#	for (dirpath, dirnames, filenames) in walk(dirct):
#		for dirName in dirnames:
			#Get files list (Same here)
			#for(dirpath, dirnames, filenames) in walk(dirpath + dirName):
	global dataFrame
	dataFrame = pd.read_csv(path)
# 	for(dirpath, dirnames, filenames) in walk(dirct):
# 		for fileName in filenames:
# 			f = dirpath + fileName
# 			#dataFrame = pd.read_csv(f, names=range(int(numCent)))
# 		#	dataFrame = pd.read_csv(f, usecols=getCol(f))
# 			global dataFrame
# 			dataFrame = pd.read_csv(f)
# # 			for freq in freqs:
# # 				dataFrame = dataFrame[(dataFrame['freqCounter'] == 2601000)]
# 				#Select columns from number 3 to number 4
# # 				dataFrame = dataFrame.ix[:,2:5]
# # 				dataFrame = dataFrame[dataFrame['centroidNo'] == 0]
# # 				break
# 			#dataFrame = dataFrame[(dataFrame['methodName'] == dataFrame.iloc[1])]
# 			break
# 		break
#			break
#		break
	return dataFrame 

#Get column names 
def getCol(fileName):
	with open(fileName) as f:
		fileLine = f.readline()	
	return fileLine.split(",")

def readFirstMethodName(fileName):
	with open(fileName) as f:
		fileLine = f.readline()	#Read column name
		fileLine = f.readline() #Read the first row of contents
		return fileLine.split(",")[0]

#Process the commands
def commandProcess(argv):
	global numCent
	global inputDir
	try:
		opts, args = getopt.getopt(argv, "hc:i:", ["centroidNum=", "inputDir="])
	except getopt.GetoptError:
		print 'genFig.py -c <Number of centroids> -i <Input directory>'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print 'test.py -c <centroidNum> -i <inputDir>'
			sys.exit()
		elif opt in ("-c", "--centroidNum"):
			numCent = arg
		elif opt in ("-i", "--inputDir"):
			inputDir = arg

#	print 'Number of centroid is: ', numCent
#	print 'Input directory is ', inputDir
def configFigure(dataFrame, filePath):
# 	xticks = axes.get_xticks()
	#print dataFrame.freqCounter.iloc[0:8]
# 	for centNo in range(0,8):
# 		df = dataFrame[(dataFrame['centroidNo'] == centNo)]
# 		ax = df[['energy']].plot()
	#Formatting
	dataFrame['freqCounter'] = dataFrame[['freqCounter']].values / 1000000.0
	#Choose the first method only
	dataFrame = dataFrame[(dataFrame['methodName'] == readFirstMethodName(filePath))]
	
	fig, ax = plt.subplots(2, sharex=False);
	fileName = filePath.split("/")[-1]
	
	counterName = []
	counterName.append(getCol(filePath)[3].strip())
	counterName.append(getCol(filePath)[4].strip())
	
	ax[0].set_title(fileName.split('.')[0])
# 	ax[0].set_xticklabels(["1.2","1.4","1.6","1.8","2.0","2.2","2.4","2.6"], rotation=0, fontsize='medium')
# 	ax[0].set_xticklabels(dataFrame['freqCounter'], rotation=0, fontsize='medium')
	
	cur_pos = []
	for i in range(2):
		ax[i].set_ylabel(counterName[i], fontsize='medium')
		cur_pos.append(ax[i].get_position())
		ax[i].set_position([cur_pos[i].x0 * 0.7, cur_pos[i].y0 + cur_pos[i].height * 0.1, cur_pos[i].width * 0.75, cur_pos[i].height * 0.9])

	#print(dataFrame['freqCounter'])
		
# 	plt.setp(ax[0], xticks=[2.6, 2.4, 2.2, 2.0, 1.8, 1.6, 1.4, 1.2])
	dataFrame.groupby("centroidNo").plot(x="freqCounter", y=getCol(filePath)[3].strip(), legend=False, ax=ax[0], xticks=dataFrame['freqCounter'])
	dataFrame.groupby("centroidNo").plot(x="freqCounter", y=getCol(filePath)[4].strip(), ax=ax[1], xticks=dataFrame['freqCounter'])
# 	box = ax.get_position()
# 	ax.set_position([box.x0 * 0.7, box.y0 + box.height * 0.1, box.width * 0.75, box.height * 0.9])
	plt.legend([v[0] for v in dataFrame.groupby('centroidNo')['centroidNo']], loc='center left', bbox_to_anchor=(1.1, 0.5))
	
# 	df = dataFrame[(dataFrame['centroidNo'] == 0)]
# 	ax = df[['energy']].plot()
# 	df1 = dataFrame[(dataFrame['centroidNo'] == 1)]
# 	ax2 = ax.twinx()
# 	ax2.plot(ax.get_xticks(), df1[['energy']], linestyle=':', marker='o')
# 	ax2.set_ylim((0,  max(df1[('energy')].values)))
# 	
# 	ax = df1[['energy']].plot()
# 		print df.iloc[:,4:5]
# 	plt.show()
	figName = path() + "data/sunflow/fig/" + fileName.split(".")[0]
# 	figName = path() + "energy_energy_counter1_branch-misses.csv".split(".")[0]
# 	figName = filePath.split(".")[0]
	print figName
	plt.savefig(figName + ".eps", format='eps')

	
def main(argv):	
	commandProcess(argv)
	filePath = path() + inputDir
	dataFrame = getDataFrame(filePath)
	configFigure(dataFrame, filePath)

if __name__ == '__main__':
	main(sys.argv[1:])
